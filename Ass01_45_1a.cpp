#include "bits/stdc++.h"
#include "unistd.h"
using namespace std;
#define SIZE 100
#define MAXVAL 5000

void fillArr (int arr[SIZE], int seedV) {
  int iter;
  srand (seedV);
  for (iter = 0; iter < SIZE; iter ++) {
    arr[iter] = (rand() % MAXVAL + 1);
  }
}
void printArr(int *arr, int size) {
  int iter;
  for (iter = 0; iter < size; iter ++)
    printf("%d ", arr[iter]);
  printf("\n");
}

void childProcess(int arr[SIZE], int arrNum, string strArrNum, int pipe[2]) {
  fillArr (arr, arrNum * SIZE);
  sort (arr, arr + SIZE);
  cout <<"\n +++ " << strArrNum << " Array on the sending end :\n\n";
  printArr(arr, SIZE);
  cout <<"\n +++ End of " << strArrNum << " Array\n";
  write (pipe[1], arr, SIZE * 4);
}

void errorInFork() {
  printf(" +++ The fork process failed! Exiting now ...\n");
  exit(1);
}

int main() {
  int sendArr[3][SIZE], getArr[3 * SIZE], x, pipes[3][2], iter;
  for (iter = 0; iter < 3; iter ++)
    pipe(pipes[iter]);
  x = fork();
  if (x == 0) {
    x = fork ();
    if (x == 0) {
      x = fork ();
      if (x == 0) {
        read(pipes[0][0], getArr, SIZE * 4);
        read(pipes[1][0], getArr + SIZE, SIZE * 4);
        read(pipes[2][0], getArr + 2 * SIZE, SIZE * 4);
        sort(getArr, getArr + 3 * SIZE);
        cout << "\n +++ Complete Array on the receiving end :\n\n";
        printArr(getArr, 3 * SIZE);
        cout << "\n +++ End of complete array\n";
      } else if (x > 0) {
        childProcess(sendArr[2], 2, "Third", pipes[2]);
      } else {
        errorInFork();
      }
    } else if (x > 0) {
      childProcess(sendArr[1], 1, "Second", pipes[1]);
    } else {
      errorInFork();
    }
  } else if (x > 0) {
    childProcess(sendArr[0], 0, "First", pipes[0]);
  } else {
    errorInFork();
  }
}
