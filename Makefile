all : pipes basicShell

pipes : Ass01_45_1a.cpp
	g++ Ass01_45_1a.cpp -o pipes

basicShell : Ass01_45_1b.cpp
	g++ Ass01_45_1b.cpp -o basicShell

clean :
	rm pipes basicShell
