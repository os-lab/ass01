# OS Lab 01

## Topic(s)
Pipes and Basic Shell

## How To Run
After cloning locally, run `make` to compile all the files.<br>

1. Ass01 gets compiled to `pipes`
2. Ass02 gets compiled to `basicShell`

Run the required file after that.<br>

Run `make clean` to clean the executable files.
