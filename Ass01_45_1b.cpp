#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include "sys/wait.h"
#define SIZE 500
#define PMT "\n --> "

void errorInFork() {
  printf(" +++ The fork process failed! Exiting now ...\n");
  exit(1);
}

int main() {
  int iter, status, i;
  pid_t x;
  char cmd[SIZE], *args[SIZE], *each;
  printf(" +++ Custom linux shell.\n\t\
  @@@ Type in the command with arguments separated by space.\
  \n\t@@@ Type quit to exit ...\n");
  while (true) {
    printf(PMT);
    scanf(" %[^\n]", cmd);
    if (strcmp(cmd, "quit") == 0) {
      printf(" +++ Signal for abort! Exiting now ...\n");
      exit(0);
    }
    each = strtok(cmd, " ");
    for (iter = 0; each != NULL;iter ++, each = strtok(NULL, " ")) {
      args[iter] = (char*) malloc(strlen(each));
      args[iter] = each;
    }
    args[iter] = (char*) NULL;
    printf("\n");
    x = fork();
    if (x == 0) {
      execvp (*args, args);
      exit(0);
    } else if (x > 0) {
      while (wait(&status) != x);
    } else {
      errorInFork();
    }
  }
}
